
Proposed exercises
-------------------

The directory ex01 contains the original working example.

Each exercise (ex02,... ex05) consists in copy the files of the corresponding directory to the main directory, overwriting original files if applicable, and modifying runcommand.c so that the tests (test-runcommand) run without errors.

ex01 : working example
ex02 : detect abnormal termination
ex03 : perform IO redirection
ex04 : detect exec failure 
ex05 : allow non-blocking execution



